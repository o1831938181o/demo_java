java.base
Defines the foundational APIs of the Java SE Platform.
java.compiler
Defines the Language Model, Annotation Processing, and Java Compiler APIs.

```text
# No Usage
-[x]java.datatransfer
Defines the API for transferring data between and within applications.
-[x]java.desktop
Defines the AWT and Swing user interface toolkits, plus APIs for accessibility, audio, imaging, printing, and JavaBeans.

-[x]java.management.rmi
Defines the RMI connector for the Java Management Extensions (JMX) Remote API.

-[x]java.rmi
Defines the Remote Method Invocation (RMI) API.

-[xP]java.scripting
Defines the Scripting API.

-[x]java.se
Defines the API of the Java SE Platform.
```

-[XP]java.management
Defines the Java Management Extensions (JMX) API.

-[xP]java.instrument
Defines services that allow agents to instrument programs running on the JVM.

-[xP]java.logging
Defines the Java Logging API.

-[xp]java.naming
Defines the Java Naming and Directory Interface (JNDI) API.

java.net.http
Defines the HTTP Client and WebSocket APIs.

-[xP]java.prefs
Defines the Preferences API.

> security No USAGE YET
```text
java.security.jgss
Defines the Java binding of the IETF Generic Security Services API (GSS-API).
java.security.sasl
Defines Java support for the IETF Simple Authentication and Security Layer (SASL).
```

> define the sql
 ```text
java.sql
Defines the JDBC API.
java.sql.rowset
Defines the JDBC RowSet API.
java.transaction.xa
Defines an API for supporting distributed transactions in JDBC.
```

> define The XML
```text
-[x]java.xml
Defines the Java API for XML Processing (JAXP), the Streaming API for XML (StAX), the Simple API for XML (SAX), and the W3C Document Object Model (DOM) API.
-[x]java.xml.crypto
Defines the API for XML cryptography.
```
