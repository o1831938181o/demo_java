package lee.pkg20211102;

public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}
