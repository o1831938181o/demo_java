plugins {
    java
    application
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

group = "com.xiaomi"
version "1.0"

repositories {
    mavenCentral()
    google()
}

application {
    mainClass.set("com.xiaomi.Main")
}

tasks {
    jar {
        archiveFileName.set("core.jar")
        manifest.attributes["Main-Class"] = "com.xiaomi.Main"
    }

    getByName<Test>("test") {
        useJUnitPlatform()
    }

    register<Copy>("install") {
        dependsOn("installDist")
        from("$rootDir/cmdline-tools")
        into("$buildDir/install/utils/bin/cmdline-tools")
    }
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    implementation("org.ow2.asm:asm:9.3")
    implementation("org.ow2.asm:asm-tree:9.3")
    implementation("org.apache.commons:commons-compress:1.21")
    implementation("com.google.code.gson:gson:2.9.0")
}