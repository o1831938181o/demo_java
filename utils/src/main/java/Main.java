import utils.CliArgs;
import utils.FileUtils;

import java.io.File;
import java.io.IOException;

public class Main {
    static void run() throws IOException {
        FileUtils.initFile();
        FileUtils.iterAndroidJar();
        FileUtils.iterFrameworkJar();
        FileUtils.afterIter();
    }

    static void usage() {
        System.out.println("Usage: create_android_sdk -f absPathOfFrameworkJar -r absPathOfFrameWorkResApk [-v] [versionOfSdk] [-n] [customName] [-o] [outputFolder]\n\tor: create_android_sdk -h to get help message");
        System.out.println("\t(to create a custom sdk without the '@hide' api)");
        System.out.println("the meaning of these arguments are:");
        System.out.println("\t-v version of SDK, support: Tiramisu, 30, 31, 32, it's better to match the rom's version.\n\t\tIt's not necessary, the default version is 'Tiramisu'");
        System.out.println("\t-f the absolute path of the 'framework.jar',\n\t\tusually it locates somewhere like '<pathOfSourceCode>/out/soong/.intermediates/frameworks/base/framework/android_common/combined/framework.jar'");
        System.out.println("\t-r the absolute path of the 'framework-res.apk',\n\t\tusually it locates somewhere like '<pathOfSourceCode>/out/soong/.intermediates/frameworks/base/core/res/framework-res/android_common/framework-res.apk'");
        System.out.println("\t-o the storage location of the final zip file.\n\t\tIt's not necessary, the default location is the current path");
        System.out.println("\t-n custom name of SDK\n\t\tIt's not necessary, the default name is 'Pangu'");
        System.out.println("\t'-h' or '--help' this help message");
    }

    public static void main(String[] args) throws IOException {
        CliArgs cliArgs = new CliArgs(args);
        if (cliArgs.getIndexes().containsKey("-h") || cliArgs.getIndexes().containsKey("--help")) {
            usage();
            return;
        }
        String pathOfFrameworkJar = cliArgs.switchValue("-f");
        String pathOfFrameWorkResApk = cliArgs.switchValue("-r");
        String version = cliArgs.switchValue("-v", "Tiramisu");
        String sdkName = cliArgs.switchValue("-n","Pangu");
        FileUtils.OUTPUT = cliArgs.switchValue("-o", FileUtils.HOME);
        try {
            FileUtils.checkNotNull(version, "'-v' parameter not provided");
            FileUtils.checkNotNull(pathOfFrameworkJar, "'-f' parameter not provided");
            FileUtils.checkNotNull(pathOfFrameWorkResApk, "'-r' parameter not provided");

            FileUtils.checkVersionValid(version);
            FileUtils.checkSDKNameValid(sdkName);
            FileUtils.FRAMEWORK_JAR = new File(pathOfFrameworkJar);
            FileUtils.FRAMEWORK_RES = new File(pathOfFrameWorkResApk);
            FileUtils.checkFileExits(FileUtils.FRAMEWORK_JAR, "path of framework.jar doesn't exit");
            FileUtils.checkFileExits(FileUtils.FRAMEWORK_RES, "path of framework-res.apk doesn't exit");

            run();
        } catch (Exception e) {
            e.printStackTrace();
            usage();
        }
    }
}
