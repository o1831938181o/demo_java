package builder;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static utils.CommandUtils.runCommand;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public final class FileBuilder {
    private File mFile;
    private File mFolderToExtract;
    private File mZipToHold;
    private File mQueryTargetFolder;
    private File mQueryRecursivelyTargetFolder;
    private String mQueryName;

    public FileBuilder(File file) {
        mFile = file;
    }

    private void changeFileToThis(File file) {
        FileUtils.checkNotNull(file, "can't assign file to a null object");
        mFile = file;
        refresh();
    }

    private void refresh() {
        mFolderToExtract = null;
        mZipToHold = null;
        mQueryTargetFolder = null;
        mQueryRecursivelyTargetFolder = null;
        mQueryName = null;
    }

    public File build() {
        return mFile;
    }

    public FileBuilder renameTo(String name) {
        String newPath = mFile.getParent() + File.separator + name;
        File newFile = new File(newPath);
        if (newFile.exists()) {
            delete(newFile);
        }
        if (!mFile.renameTo(new File(newPath))) {
            throw new RuntimeException("rename failed");
        }
        changeFileToThis(newFile);
        return this;
    }

    public FileBuilder makeJarHere() {
        String path = mFile.getAbsolutePath();
        String cmd = "cd " + path + " && jar -cvf android.jar *";
        runCommand(cmd);
        return queryNameHere("android.jar");
    }

    private FileBuilder copy(Path source, Path target) {
        FileUtils.checkNotNull(source, "source file has not been specified");
        FileUtils.checkNotNull(target, "target file has not been specified");
        // check if target is a directory
        boolean isDir = Files.isDirectory(target);
        File sourceToFile = source.toFile();

        if (sourceToFile.isFile()) {
            copySingleFileToFolder(sourceToFile, isDir, target);
        } else {
            File[] fileList = sourceToFile.listFiles();
            if (fileList == null) {
                changeFileToThis(target.toFile());
                return this;
            }
            for (File sFile : fileList) {
                copySingleFileToFolder(sFile, isDir, target);
            }
        }
        changeFileToThis(target.toFile());
        return this;
    }

    private void copySingleFileToFolder(File singleFile, boolean isDir, Path target) {
        Path sPath = singleFile.toPath();
        Path dest = (isDir) ? target.resolve(sPath.getFileName()) : target;
        EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
        TreeCopier tc = new TreeCopier(sPath, dest);
        try {
            Files.walkFileTree(sPath, opts, Integer.MAX_VALUE, tc);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public FileBuilder copyAndReplaceFolder(File fromFolder, File toFolder) {
        return copy(fromFolder.toPath(), toFolder.toPath());
    }

    public FileBuilder copyToReplace(File toFolder) {
        return copy(mFile.toPath(), toFolder.toPath());
    }

    public FileBuilder singleChild() {
        File[] files = mFile.listFiles();
        if (files != null && files.length == 1) {
            changeFileToThis(files[0]);
        }
        return this;
    }


    /**
     * do compress.
     * <p>need to specify where to compress by method {@link #compressHere()} or {@link #compressTo(String)} first,
     * if the specified zip file exists, it will be deleted first
     *
     * @return compressed file
     */
    private FileBuilder compress() {
        FileUtils.checkNotNull(mZipToHold, "the file to store zip has not been specified");
        System.out.println("compressing " + mFile.getAbsolutePath());
        delete(mZipToHold);
        List<File> fileArrayList = new ArrayList<>();
        getFiles(mFile, fileArrayList);
        try (ZipArchiveOutputStream zipArchiveOutputStream = new ZipArchiveOutputStream(new FileOutputStream(mZipToHold))) {
            for (File file : fileArrayList) {
                ArchiveEntry entry = zipArchiveOutputStream.createArchiveEntry(file, entryName(file, mFile));
                zipArchiveOutputStream.putArchiveEntry(entry);
                if (file.isFile()) {
                    try (InputStream inputStream = Files.newInputStream(file.toPath())) {
                        IOUtils.copy(inputStream, zipArchiveOutputStream);
                    }
                }
                zipArchiveOutputStream.closeArchiveEntry();
            }
            zipArchiveOutputStream.finish();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("compress succeed in: " + mZipToHold);
        changeFileToThis(mZipToHold);
        return this;
    }

    /**
     * do extract.
     * <p>need to specify where to extract by method {@link #extractHere()} or {@link #extractTo(String)} first,
     * if the specified folder exists, it will be deleted first
     *
     * @return the extracted directory
     */
    private FileBuilder extract() {
        FileUtils.checkNotNull(mFolderToExtract, "the directory to extract to has not been specified");
        if (!supportSuffixToExtract(mFile)) {
            throw new IllegalArgumentException("can not extract file endsWith" + getSuffix(mFile));
        }
        System.out.println("extract to " + mFolderToExtract.getAbsolutePath());

        String fileNameWithoutSuffix = getFileNameWithoutSuffix(mFile);
        File zipRootFolder = new File(mFolderToExtract + File.separator + getFileNameWithoutSuffix(mFile));
        delete(zipRootFolder);
        File zipExtractPath = null;

        try (ZipFile zipFile = new ZipFile(mFile)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (zipExtractPath == null) {
                    zipExtractPath = entry.getName().startsWith(fileNameWithoutSuffix) ? mFolderToExtract : zipRootFolder;
                }
                File f = new File(zipExtractPath, entry.getName());
                if (entry.isDirectory()) {
                    if (!isValidDestPath(mFolderToExtract.getAbsolutePath(), f.getAbsolutePath())) {
                        throw new IOException("Final directory output path is invalid: " + f.getAbsolutePath());
                    }
                    if (!f.isDirectory() && !f.mkdirs()) {
                        throw new IOException("failed to create directory " + f);
                    }
                } else {
                    File parent = f.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("failed to create directory " + parent);
                    }
                    try (InputStream inputStream = zipFile.getInputStream(entry);
                         FileOutputStream outputStream = new FileOutputStream(f)) {
                        IOUtils.copy(inputStream, outputStream);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("failed to extract file " + mFolderToExtract.getName() + " " + e);
        }

//        try (ZipArchiveInputStream inputStream = new ZipArchiveInputStream(new FileInputStream(mFile))) {
//            ZipArchiveEntry entry;
//            while ((entry = inputStream.getNextZipEntry()) != null) {
//                if (zipExtractPath == null) {
//                    zipExtractPath = entry.getName().startsWith(getFileNameWithoutSuffix(mFile)) ? mFolderToExtract : zipRootFolder;
//                }
//                File f = new File(zipExtractPath, entry.getName());
//                if (entry.isDirectory()) {
//                    if (!f.isDirectory() && !f.mkdirs()) {
//                        throw new IOException("failed to create directory " + f);
//                    }
//                } else {
//                    File parent = f.getParentFile();
//                    if (!parent.isDirectory() && !parent.mkdirs()) {
//                        throw new IOException("failed to create directory " + parent);
//                    }
//                    try (OutputStream o = Files.newOutputStream(f.toPath())) {
//                        IOUtils.copy(inputStream, o);
//                    }
//                }
//            }
//        } catch (IOException e) {
//            throw new RuntimeException("failed to extract file " + mFolderToExtract.getName() + " " + e);
//        }
        System.out.println("extract succeed in: " + zipRootFolder.getAbsolutePath());
        changeFileToThis(zipRootFolder);
        return this;
    }

    private static boolean isValidDestPath(String targetDir, String destPathStr) {
        // validate the destination path of a ZipFile entry,
        // and return true or false telling if it's valid or not.

        Path destPath = Paths.get(destPathStr);
        Path destPathNormalized = destPath.normalize(); //remove ../../ etc.

        return destPathNormalized.toString().startsWith(targetDir + File.separator);
    }

    private FileBuilder query() {
        FileUtils.checkNotNull(mQueryName, "the query name has not been specified");
        boolean recursive = false;
        if (mQueryTargetFolder == null) {
            recursive = true;
            if (mQueryRecursivelyTargetFolder == null) {
                throw new RuntimeException("the directory to query from has not been specified");
            }
        }
        if ((!recursive && !mQueryTargetFolder.exists()) || (recursive && !mQueryRecursivelyTargetFolder.exists())) {
            throw new IllegalArgumentException("the directory to query from doesn't exist");
        }
        if (!recursive) {
            File[] fileList = mQueryTargetFolder.listFiles();
            if (fileList == null) {
                return this;
            }
            for (File f : fileList) {
                if (mQueryName.equals(f.getName())) {
                    changeFileToThis(f);
                    return this;
                }
            }
            return null;
        } else {
            return queryFileInFolder(mQueryName, mQueryRecursivelyTargetFolder);
        }

    }

    private static boolean supportSuffixToExtract(File file) {
        String[] supported = {".zip", ".jar", ".apk"};
        String fileName = file.getName();
        for (String s : supported) {
            if (fileName.endsWith(s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * not recursive query, only query the current directory
     *
     * @param folder the folder to query recursively from
     * @return this FileBuilder object
     */
    public FileBuilder queryNameInFolder(String fileName, File folder) {
        mQueryTargetFolder = folder;
        mQueryName = fileName;
        return query();
    }

    public FileBuilder queryThisNameInFolder(File folder) {
        mQueryTargetFolder = folder;
        mQueryName = mFile.getName();
        return query();
    }

    public FileBuilder queryNameHere(String fileName) {
        mQueryTargetFolder = mFile;
        mQueryName = fileName;
        return query();
    }

    public FileBuilder queryNameRecursivelyHere(String fileName) {
        mQueryRecursivelyTargetFolder = mFile;
        mQueryName = fileName;
        return query();
    }

    /**
     * recursive query under given directory
     *
     * @param folder the folder to query from
     * @return this FileBuilder object
     */
    public FileBuilder queryNameRecursivelyInFolder(File folder) {
        mQueryRecursivelyTargetFolder = folder;
        return query();
    }

    /**
     * compress to a zip file in the same directory as the given file,
     * the target file has the same name as the given file and ends with ".zip"
     *
     * @return the target zip file
     */
    public FileBuilder compressHere() {
        mZipToHold = new File(mFile.getAbsolutePath() + ".zip");
        return compress();
    }

    /**
     * compress to a specified zip file
     *
     * @param zipFilePath the zip file to hold the target file, must end with ".zip"
     * @return the target zip file
     */
    public FileBuilder compressTo(String zipFilePath) {
        if (!(new File(zipFilePath).exists())) {
            throw new RuntimeException("the zip folder doesn't exist");
        }
        mZipToHold = new File(zipFilePath, mFile.getName() + ".zip");
        return compress();
    }

    /**
     * extract to current directory
     *
     * @return this FileBuilder object
     */
    public FileBuilder extractHere() {
        mFolderToExtract = mFile.getParentFile();
//        if (mFile.getName().endsWith(".apk")) {
//            return unzipApkFileHere();
//        }
        return extract();
    }

    /**
     * extract to a specified directory
     *
     * @param folder the specified directory
     * @return this FileBuilder object
     */
    public FileBuilder extractTo(String folder) {
        mFolderToExtract = new File(folder);
//        if (mFile.getName().endsWith(".apk")) {
//            return unzipApkFileTo(mFolderToExtract);
//        }
        return extract();
    }

    private FileBuilder unzipApkFileHere() {
        String fileName = mFile.getName();
        String unzipFolder = mFile.getParent() + File.separator + fileName.substring(0, fileName.lastIndexOf("."));
        delete(new File(unzipFolder));
        return unZipInternal(mFile.getName(), mFile.getParent(), unzipFolder);
    }

    private FileBuilder unzipApkFileTo(File folder) {
        String fileName = mFile.getName();
        String unZipFolder = folder.getAbsolutePath() + File.separator + fileName.substring(0, fileName.lastIndexOf("."));
        delete(new File(unZipFolder));
        return unZipInternal(mFile.getName(), mFile.getParent(), unZipFolder);
    }

    private FileBuilder unZipInternal(String fileName, String parentPath, String unZipFolder) {
        if (!fileName.endsWith(".apk")) {
            throw new IllegalArgumentException("must end with \".apk\"");
        }
        String cmd = "cd " + parentPath + " && unzip " + fileName + " -d " + unZipFolder;
        runCommand(cmd);
        return new FileBuilder(new File(unZipFolder));
    }

    public FileBuilder getSDKWithSDKManager(String sdkManagerFolderPath, String toFolderPath, String version) {
        sdkManagerFolderPath = mFile.getAbsolutePath();
        String cmd = "cd " + sdkManagerFolderPath + " && ./sdkmanager --sdk_root=" + toFolderPath + " --update " +
                "&& echo y | ./sdkmanager --sdk_root=" + toFolderPath + " --install \"platforms;android-" + version + "\"";
        runCommand(cmd);
        return queryFileInFolder("platforms", new File(toFolderPath))
                .singleChild();
    }

    /**
     * @param file delete the spec file
     */
    public static void delete(File file) {
        if (file == null || !file.exists()) {
            return;
        }
        try {
            Files.walkFileTree(file.toPath(), new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param folder_dir folder/file we are using to get all files
     * @param res        holder for the files
     */
    private static void getFiles(File folder_dir, List<File> res) {
        if (folder_dir == null || !folder_dir.exists()) return;
        if (folder_dir.isFile()) {
            res.add(folder_dir);
            return;
        }
        File[] files = folder_dir.listFiles();
        for (File file : Objects.requireNonNull(files)) {
            if (file.isFile()) {
                res.add(file);
            }
            if (file.isDirectory()) {
                getFiles(file, res);
            }
        }
    }

    /**
     * @param file     file/dir relative path according to the root_dir
     * @param root_dir root_dir [cd .]
     * @return relative path
     */
    private static String entryName(File file, File root_dir) {
        FileUtils.checkNotNull(file, "file can't be null");
        FileUtils.checkNotNull(root_dir, "root_dir can't be null");
        if (!root_dir.isDirectory()) {
            throw new RuntimeException("root_dir should be a dir");
        }
        return root_dir.toPath().relativize(file.toPath()).toString();
    }

    /**
     * @param file file we are getting the suffix
     * @return the suffix string such as sample.txt -> txt
     */
    private static String getSuffix(File file) {
        if (file.isDirectory()) {
            throw new RuntimeException("can not acquire suffix from a directory");
        }
        String fileName = file.getName();
        int idx = fileName.lastIndexOf(".");
        if (idx < 0) {
            return "";
        }
        return fileName.substring(fileName.lastIndexOf("."));
    }

    /**
     * @param file file/dir we are getting the name with
     * @return the prefix string such as sample.txt -> sample
     */
    private static String getFileNameWithoutSuffix(File file) {
        String fileName = file.getName();
        int idx = fileName.lastIndexOf(".");
        if (idx < 0) {
            return fileName;
        }
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

    /**
     * @param name   file or folder's name
     * @param folder query from
     * @return file in folder for find, null for not find
     */
    private FileBuilder queryFileInFolder(String name, File folder) {
        if (folder == null || !folder.exists()) {
            return this;
        }
        File[] files = folder.listFiles();
        if (files == null) {
            return this;
        }
        for (File f : files) {
            if (f.getName().equals(name)) {
                changeFileToThis(f);
                return this;
            }
            if (f.isDirectory()) {
                queryFileInFolder(name, f);
            }
        }
        return this;
    }

    /**
     * @param source the path to the file
     * @param target the output stream to write to
     */
    private void copyFile(Path source, Path target) {
        CopyOption[] options = new CopyOption[]{REPLACE_EXISTING};
        try {
            Files.copy(source, target, options);
        } catch (IOException x) {
            throw new RuntimeException("Unable to copy: " + source + " " + x);
        }
    }

    /**
     * custom FileVisitor
     * for copy only
     */
    private class TreeCopier implements FileVisitor<Path> {
        private final Path source;
        private final Path target;

        TreeCopier(Path source, Path target) {
            this.source = source;
            this.target = target;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            // before visiting entries in a directory we copy the directory
            Path newdir = target.resolve(source.relativize(dir));
            try {
                Files.copy(dir, newdir);
            } catch (FileAlreadyExistsException x) {
                // ignore
            } catch (IOException x) {
                System.err.format("Unable to create: %s: %s%n", newdir, x);
                return SKIP_SUBTREE;
            }
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            copyFile(file, target.resolve(source.relativize(file)));
            return CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) {
            if (exc instanceof FileSystemLoopException) {
                System.err.println("cycle detected: " + file);
            } else {
                System.err.format("Unable to copy: %s: %s%n", file, exc);
            }
            return CONTINUE;
        }
    }

}

