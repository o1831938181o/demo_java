package utils;



import asm.VisitFile;
import builder.FileBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;

public final class FileUtils {
    /**
     * the holder dir
     */
    public static final String HOME;
    private static final String RAW;
    private static final String CMD_LINE_TOOLS;

    private static final Set<String> SUPPORT_VERSION;

    private static String SDK_NAME;

    static {
        HOME = System.getProperty("user.dir");
        RAW = HOME + File.separator + "raw";
        CMD_LINE_TOOLS = HOME + File.separator + "cmdline-tools";
        SUPPORT_VERSION = new HashSet<>(Arrays.asList("30", "31", "32", "Tiramisu"));
    }

    public static File FRAMEWORK_JAR;
    public static File FRAMEWORK_RES;
    public static String OUTPUT;
    private static String VERSION;

    private static File mPanguFolder;
    private static File mAndroidInAndroidJar;
    private static File mAndroidInFrameworkJar;

    /**
     * the iter instance (should not be changed to multi-thread)
     */
    public static IterEvent iterEvent;


    private static File getSDK() {
        String sdkManagerFolder = CMD_LINE_TOOLS + File.separator + "bin";
        return new FileBuilder(new File(sdkManagerFolder)).getSDKWithSDKManager(sdkManagerFolder, RAW, VERSION).build();
    }


    private static File getPanguFolder() {
        return new FileBuilder(getSDK())
                .singleChild()
                .renameTo("android-" + SDK_NAME)
                .build();
    }

    private static File getAndroidInAndroidJar(File panguFolder) {
        return new FileBuilder(panguFolder)
                .queryNameHere("android.jar")
                .extractTo(RAW)
                .queryNameHere("android")
                .build();
    }

    private static File getAndroidInFrameworkJar() {
        return new FileBuilder(FRAMEWORK_JAR)
                .extractTo(RAW)
                .queryNameHere("android")
                .build();
    }

    public static void initFile() {
        mPanguFolder = getPanguFolder();
        mAndroidInAndroidJar = getAndroidInAndroidJar(mPanguFolder);
        mAndroidInFrameworkJar = getAndroidInFrameworkJar();

        FileBuilder.delete(new FileBuilder(mPanguFolder)
                .queryNameHere("android.jar")
                .build());

        File packageXml = new FileBuilder(mPanguFolder)
                .queryNameHere("package.xml")
                .build();
        XmlUtils.replaceToSpecName(SDK_NAME, packageXml);

        new FileBuilder(FRAMEWORK_RES)
                .extractTo(RAW)
                .copyToReplace(mPanguFolder);
    }

    public static void afterIter() {
        new FileBuilder(new File(RAW))
                .queryNameHere("android")
                .makeJarHere()
                .copyToReplace(mPanguFolder)
                .compressTo(OUTPUT);
        FileBuilder.delete(new FileBuilder(new File(RAW)).build());
    }

    /**
     * the iter interface
     */
    public interface IterEvent {
        void ifDir(File f);

        void ifFile(File f) throws IOException;
    }

    /**
     * @param dir the file (maybe file or a dir)
     * @throws IOException
     */
    public static void visitAllDirsAndFiles(File dir) throws IOException {
        checkNotNull(dir, "dir can't be null");
        if (!dir.exists()) throw new RuntimeException("real dir does not exist");
        checkNotNull(iterEvent, "iterEvent can't be null");
        if (dir.isDirectory()) {
            iterEvent.ifDir(dir);
            String[] children = dir.list();
            if (children == null) return;
            for (String child : children) {
                visitAllDirsAndFiles(new File(dir, child));
            }
        } else {
            iterEvent.ifFile(dir);
        }
    }

    /**
     * @param f the file that in ./raw/android/android
     * @return file-null if not accord to find
     */
    public static File findFileInFrameworkJar(File f) {
        checkNotNull(f);
        Path source = mAndroidInAndroidJar.toPath();
        Path target = mAndroidInFrameworkJar.toPath();
        return target.resolve(source.relativize(f.toPath())).toFile();
    }

    /**
     * @param f the file that in ./raw/framework/android
     * @return file-null if not accord to find
     */
    public static File findFileInAndroidJar(File f) {
        checkNotNull(f);
        Path source = mAndroidInFrameworkJar.toPath();
        Path target = mAndroidInAndroidJar.toPath();
        return target.resolve(source.relativize(f.toPath())).toFile();
    }

    /**
     * Creates any necessary but nonexistent parent directories of the specified file. Note that if
     * this operation fails it may have succeeded in creating some (but not all) of the necessary
     * parent directories.
     *
     * @throws IOException if an I/O error occurs, or if any necessary but nonexistent parent
     *                     directories of the specified file could not be created.
     * @since 4.0
     */
    public static void createParentDirs(File file) throws IOException {
        checkNotNull(file);
        File parent = file.getCanonicalFile().getParentFile();
        if (parent == null) {
            /*
             * The given directory is a filesystem root. All zero of its ancestors exist. This doesn't
             * mean that the root itself exists -- consider x:\ on a Windows machine without such a drive
             * -- or even that the caller can create it, but this method makes no such guarantees even for
             * non-root files.
             */
            return;
        }

        if (!parent.isDirectory() && !parent.mkdirs()) {
            throw new IOException("Unable to create parent directories of " + file);
        }
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference an object reference
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> void checkNotNull(T reference, String... msg) {
        if (reference == null) throw new NullPointerException(String.join("\n", msg));
    }

    public static void checkFileExits(File file, String... msg) {
        if (file == null || !file.exists()) {
            throw new NullPointerException(String.join("\n", msg));
        }
    }

    public static void checkVersionValid(String v) {
        if (!SUPPORT_VERSION.contains(v)) {
            throw new IllegalArgumentException("don't support this version: " + v + "\nshould be one of these: " + SUPPORT_VERSION + "\ndefault version is:Tiramisu");
        }
        VERSION = v;
    }

    public static void checkSDKNameValid(String name) {
        Pattern pattern = Pattern.compile("[A-Z]\\w*$");
        if (!pattern.matcher(name).matches()) {
            throw new IllegalArgumentException("don't support this sdk name: " + name);
        }
        SDK_NAME = name;
    }

    public static void iterAndroidJar() throws IOException {
        FileUtils.checkNotNull(mAndroidInAndroidJar);
        FileUtils.iterEvent = new FileUtils.IterEvent() {
            final Path target = mAndroidInAndroidJar.toPath();

            @Override
            public void ifDir(File f) {
                // Do Nothing
            }

            @Override
            public void ifFile(File f) throws IOException {
                if (!f.getName().endsWith(".class")) return;
                File fileInFrameworkJar = findFileInFrameworkJar(f);
                // relative path exclude android
                File output = target.resolve(mAndroidInAndroidJar.toPath().relativize(f.toPath())).toFile();
                VisitFile.rewriteClass(f, fileInFrameworkJar, output); // choose the default output
            }
        };
        FileUtils.visitAllDirsAndFiles(mAndroidInAndroidJar);
    }

    public static void iterFrameworkJar() throws IOException {
        FileUtils.checkNotNull(mAndroidInFrameworkJar);
        FileUtils.iterEvent = new FileUtils.IterEvent() {
            final Path target = mAndroidInAndroidJar.toPath();

            @Override
            public void ifDir(File f) {
                // Do Nothing
            }

            @Override
            public void ifFile(File f) throws IOException {
                if (!f.getName().endsWith(".class")) return;
                if (!findFileInAndroidJar(f).exists()) {
                    // relative path exclude android
                    File output = target.resolve(mAndroidInFrameworkJar.toPath().relativize(f.toPath())).toFile();
                    VisitFile.rewriteClass(f, output);
                }
            }
        };
        FileUtils.visitAllDirsAndFiles(mAndroidInFrameworkJar);
    }

}

