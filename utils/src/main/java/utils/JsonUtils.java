package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Map;

public class JsonUtils {

    private final static Gson gson;
    private final static Type mType;

    static {
        gson = new Gson();
        mType = new TypeToken<Map<String, String>>() {
        }.getType();
    }

    public static String getUrl(String key) {

        String url = "https://dl.google.com/android/repository/platform-31_r01.zip";

        try {
            String fileName = FileUtils.HOME + File.separator + "sites.json";
            Map<String, String> m = gson.fromJson(new FileReader(fileName), mType);
            if (m.get(key) == null) throw new IllegalArgumentException();
            url = m.get(key);
        } catch (FileNotFoundException | IllegalArgumentException e) {
            System.err.println("default set to Android-platform-31");
            e.printStackTrace();
        }
        return url;
    }
}
