package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class CommandUtils {
    public static void runCommand(String cmd) {
        System.out.println("cmd: " + cmd);
        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/bash", "-c", cmd);
            final Process p = pb.start();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("cmd failed" + e);
        }
    }
}
