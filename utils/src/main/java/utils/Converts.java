package utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Converts {
    /**
     * @param obj obj that hold the data
     * @return the converted list
     */
    public static List<?> convertObjectToList(Object obj) {
        List<?> list = null;
        if (obj.getClass().isArray()) {
            list = Arrays.asList((Object[]) obj);
        } else if (obj instanceof Collection) {
            list = List.copyOf((Collection<?>) obj);
        }
        return list;
    }
}
