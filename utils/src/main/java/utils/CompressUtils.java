package utils;

import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * unused, deleted later
 */

public class CompressUtils {
    public static void unZip(File fileToUnZip, File folderToExtract) {
        if (folderToExtract == null) {
            throw new RuntimeException("the directory to extract to has not been specified");
        }

        String fileNameWithoutSuffix = getFileNameWithoutSuffix(fileToUnZip);
        File zipRootFolder = new File(folderToExtract, fileNameWithoutSuffix);
        File zipExtractPath = null;

        try (ZipFile zipFile = new ZipFile(fileToUnZip)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (zipExtractPath == null) {
                    zipExtractPath = entry.getName().startsWith(fileNameWithoutSuffix) ? folderToExtract : zipRootFolder;
                }
                File f = new File(zipExtractPath, entry.getName());
                if (entry.isDirectory()) {
                    if (!isValidDestPath(folderToExtract.getAbsolutePath(), f.getAbsolutePath())) {
                        throw new IOException("Final directory output path is invalid: " + f.getAbsolutePath());
                    }
                    if (!f.isDirectory() && !f.mkdirs()) {
                        throw new IOException("failed to create directory " + f);
                    }
                } else {
                    File parent = f.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("failed to create directory " + parent);
                    }
                    try (InputStream inputStream = zipFile.getInputStream(entry);
                         FileOutputStream outputStream = new FileOutputStream(f)) {
                        IOUtils.copy(inputStream, outputStream);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean isValidDestPath(String targetDir, String destPathStr) {
        // validate the destination path of a ZipFile entry,
        // and return true or false telling if it's valid or not.

        Path destPath = Paths.get(destPathStr);
        Path destPathNormalized = destPath.normalize(); //remove ../../ etc.

        return destPathNormalized.toString().startsWith(targetDir + File.separator);
    }

    private static String getFileNameWithoutSuffix(File file) {
        String fileName = file.getName();
        int idx = fileName.lastIndexOf(".");
        if (idx < 0) {
            return fileName;
        }
        return fileName.substring(0, fileName.lastIndexOf("."));
    }
}

