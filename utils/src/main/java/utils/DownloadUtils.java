package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class DownloadUtils {
    private static final HttpClient client;

    static {
        client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1).build();
    }

    /**
     * @param url      下载连接
     * @param file     下载的文件
     * @param listener 下载监听
     */

    public static void download(final String url, final File file,
                                final OnDownloadListener listener) {
        executeAsync(url, file, listener);
    }

    private static void executeAsync(final String url, final File file, final OnDownloadListener listener) {
        var r = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
        client.sendAsync(r, HttpResponse.BodyHandlers.ofInputStream())
                .thenAccept(res -> {
                    byte[] buf = new byte[2048];
                    try (InputStream is = res.body()) {
                        long total = Long.parseLong(res.headers().map().get("content-length").get(0));
                        int len;
                        FileUtils.createParentDirs(file); // create the parent file
                        try (FileOutputStream fos = new FileOutputStream(file)) {
                            long sum = 0;
                            while ((len = is.read(buf)) != -1) {
                                fos.write(buf, 0, len);
                                sum += len;
                                //下载中更新进度条
                                int progress = (int) (sum * 1.0f / total * 100);
                                //下载中更新进度条
                                listener.onDownloading(progress);
                            }
                            fos.flush();
                            //下载完成
                            listener.onDownloadSuccess(file);
                        }
                    } catch (IOException e) {
                        listener.onDownloadFailed(e);
                    }
                }).join();
    }


    public interface OnDownloadListener {

        /**
         * 下载成功之后的文件
         */
        void onDownloadSuccess(File file);

        /**
         * 下载进度
         */
        void onDownloading(int progress);

        /**
         * 下载异常信息
         */

        void onDownloadFailed(Exception e);
    }
}
