package utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class XmlUtils {
    private static final DocumentBuilderFactory factory;

    private static final TransformerFactory tff;

    static {
        factory = DocumentBuilderFactory.newInstance();
        tff = TransformerFactory.newInstance();
    }


    /**
     * @param replace  replaced thing
     * @param file input-file to be modified
     */
    public static void replaceToSpecName(String replace, File file) {
        FileUtils.checkNotNull(replace, "input key can't be null");
        FileUtils.checkNotNull(file, "input file can't be null");
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(file);
            Node node = document.getElementsByTagName("localPackage").item(0);
            Node attr = node.getAttributes().getNamedItem("path");
            String[] splitArr = attr.getNodeValue().split("-");
            splitArr[splitArr.length - 1] = replace;
            attr.setNodeValue(String.join("-", splitArr));
            saveToFile(document, file);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            System.err.println("XML : Failed Parse File " + file.getAbsolutePath());
            e.printStackTrace();
        }
    }

    public static void saveToFile(Document document, File file) throws IOException {
        FileUtils.checkNotNull(document, "document can't be null");
        FileUtils.checkNotNull(file, "input file can't be null");
        FileUtils.createParentDirs(file); // avoid file parent directory not created
        try {
            Transformer tf = tff.newTransformer();
            tf.transform(new DOMSource(document), new StreamResult(file));
        } catch (TransformerException e) {
            System.err.println("XML : Failed save to File " + file.getAbsolutePath());
            e.printStackTrace();
        }
    }
}
