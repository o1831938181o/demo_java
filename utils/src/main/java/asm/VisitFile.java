package asm;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;
import utils.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public final class VisitFile {
    /**
     * @param base     base-reader : from android.jar
     * @param advanced advanced-reader : from framework.jar
     * @return fixed-node : based android.jar but add framework.jar hide methods and fields
     */
    public static ClassNode rewriteClassNode(ClassReader base, ClassReader advanced) {
        FileUtils.checkNotNull(base);
        FileUtils.checkNotNull(advanced);
        ClassNode baseNode = new ClassNode(Opcodes.ASM7);
        ClassNode advancedNode = new ClassNode(Opcodes.ASM7);

        base.accept(baseNode, 0);
        advanced.accept(advancedNode, 0);

        if (base == advanced) return baseNode;

        Map<String, MethodNode> methodNodeMap = new HashMap<>();
        Map<String, FieldNode> fieldNodeMap = new HashMap<>();

        for (MethodNode methodNode : baseNode.methods) {
            methodNodeMap.put(methodNode.name, methodNode);
        }

        for (FieldNode fieldNode : baseNode.fields) {
            fieldNodeMap.put(fieldNode.name, fieldNode);
        }

        for (MethodNode methodNode : advancedNode.methods) {
            if (!methodNodeMap.containsKey(methodNode.name)) {
                methodNode.instructions = new InsnList();
                baseNode.methods.add(methodNode);
            }
        }

        for (FieldNode fieldNode : advancedNode.fields) {
            if (!fieldNodeMap.containsKey(fieldNode.name)) {
                baseNode.fields.add(fieldNode);
            }
        }

        return baseNode;

    }

    /**
     * @param base the baseReader from framework.jar
     * @return fixed-node without func-body
     */
    public static ClassNode rewriteClassNode(ClassReader base) {
        FileUtils.checkNotNull(base);
        ClassNode baseNode = new ClassNode(Opcodes.ASM7);
        base.accept(baseNode, 0);
        for (MethodNode methodNode : baseNode.methods) {
            methodNode.instructions = new InsnList();
        }
        return baseNode;
    }

    /**
     * @param base     base file : from android.jar
     * @param advanced may be null : from framework.jar (if framework.jar don't have ,but this case is rare)
     * @param output   output file
     * @throws IOException
     */

    public static void rewriteClass(File base, File advanced, File output) throws IOException {
        FileUtils.checkNotNull(base);
        FileUtils.checkNotNull(output, "output file must not be null");
        ClassReader baseReader = new ClassReader(new FileInputStream(base));
        ClassReader advancedReader;
        if (advanced == null || !advanced.exists()) {
            advancedReader = baseReader;
        } else {
            advancedReader = new ClassReader(new FileInputStream(advanced));
        }
        ClassNode rewrittenClassNode = rewriteClassNode(baseReader, advancedReader);
        writeClassNodeToFile(rewrittenClassNode, base, output);
    }

    /**
     * @param f      : base file from framework.jar
     * @param output : output file
     * @throws IOException
     */
    public static void rewriteClass(File f, File output) throws IOException {
        FileUtils.checkNotNull(f);
        FileUtils.checkNotNull(output, "output file must not be null");
        ClassReader baseReader = new ClassReader(new FileInputStream(f));
        ClassNode rewrittenClassNode = rewriteClassNode(baseReader);
        writeClassNodeToFile(rewrittenClassNode, f, output);
    }

    /**
     * @param node input-node
     * @param info info we are going to accord
     * @param file file to write
     * @throws IOException
     */
    public static void writeClassNodeToFile(ClassNode node, File info, File file) throws IOException {
        FileUtils.checkNotNull(node);
        FileUtils.checkNotNull(info);
        FileUtils.checkNotNull(file, "output file must not be null");
        ClassWriter cw = new ClassWriter(0);
        node.accept(cw);
        byte[] code = cw.toByteArray();
        // if we not have the parent dir we should create
        FileUtils.createParentDirs(file);

        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(code);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
