package com.peter.wweb.handler;

import com.peter.wweb.models.City;
import com.peter.wweb.repos.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Component
public class CityHandler {

    int a = 0; // thread problem when multi-thread compution
    // 44831

    private final CityRepository cityRepository;

    @Autowired
    public CityHandler(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }


    public City findCityById(Long id) {
        System.out.println("findCityById " + Thread.currentThread().getId());
        return cityRepository.findCityById(id);
    }

    public Collection<City> findAllCity() {
        a++;
        System.out.println(a);
        return cityRepository.findAll();
    }
}

