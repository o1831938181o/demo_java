package com.peter.wweb.controller;


import com.peter.wweb.handler.CityHandler;
import com.peter.wweb.models.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/city")
public class CityWebFluxController {

    @Autowired
    private CityHandler cityHandler;

    @GetMapping(value = "/{id}")
    public City findCityById(@PathVariable("id") Long id) {
        System.out.println("MonofindCityById " + Thread.currentThread().getId());
        return cityHandler.findCityById(id);
    }

    @GetMapping()
    public Collection<City> findAllCity() {
        return cityHandler.findAllCity();
    }
}
