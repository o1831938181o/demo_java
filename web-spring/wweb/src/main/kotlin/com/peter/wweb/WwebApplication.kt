package com.peter.wweb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WwebApplication

fun main(args: Array<String>) {
	runApplication<WwebApplication>(*args)
}
