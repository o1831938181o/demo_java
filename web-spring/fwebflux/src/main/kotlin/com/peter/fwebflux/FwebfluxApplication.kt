package com.peter.fwebflux

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FwebfluxApplication

fun main(args: Array<String>) {
	println("main " + Thread.currentThread().id)
	runApplication<FwebfluxApplication>(*args)
}
