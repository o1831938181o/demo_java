package flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

//上流函数
fun simpleFlow() = flow {
    for (i in 1..3) {
        println(Thread.currentThread().id)
        println("====")
        delay(1000)
        emit(i)
    }
}

fun main() = runBlocking<Unit> {
    // this: CoroutineScope
    launch {
        // 在 runBlocking 作用域中启动一个新协程
        delay(1000L)
        println("World! ${Thread.currentThread().name}")
    }
    simpleFlow().collect {
        println("Hello, ${Thread.currentThread().name}")
    }
    println("ending")
}

//runBlocking 是会阻塞主线程的，直到 runBlocking 内部全部子任务执行完毕，才会继续执行下一步的操作！


//fun main() {
//    runBlocking {
//        launch {
//            // 在后台启动一个新的协程并继续
//            delay(3000L)
//            println("World!")
//        }
//    }
//    println("Hello,")
//}
