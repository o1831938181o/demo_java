package coroutine

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.selects.SelectClause1
import kotlinx.coroutines.selects.select
import java.io.File

//@OptIn(DelicateCoroutinesApi::class)
fun main() = runBlocking {
//    println("hello world")
//
////    GlobalScope.launch {
////        repeat(7) {
////            println("12313123123")
////        }
////    }
//
////    val channel = Channel<Int>()
////    launch {
////        for (x in 1..5) {
////            channel.send(x * x)
////            delay(1000L)
////        }
////    }
//
//
//    var cur = numbersFrom(2)
////    repeat(10) {
////        val prime = cur.receive()
////        println(prime)
//////        cur = filter(cur, prime)
////    }
//
//    for (y in cur) println(y)
//    println("Done!")
//    coroutineContext.cancelChildren() // cancel all children to let main finish


    /**
     * new start - 0
     */

//    select {
//
//    }


    /**
     * select chanel
     */

    val a = getUserFromApi("")

    val b = getUserFromLocal("")

   val strr =  select<String> {
        a.onAwait{
            println("aab   $it")
            it
        }

        b.onAwait {
            println("bba")
            it
        }

    }

    println(strr)
}

// produce or actor method chanel autoclose
fun CoroutineScope.numbersFrom(start: Int) = produce<Int>() {
    var x = start
//    while (true) // infinite stream of integers from start
    repeat(10) {
        send(x++)
        delay(100L)
    }
}

fun CoroutineScope.filter(numbers: ReceiveChannel<Int>, prime: Int) = produce<Int> {
    for (x in numbers) if (x % prime != 0) send(x)
}


fun CoroutineScope.getUserFromApi(login: String) = async(Dispatchers.IO) {
    delay(100L)
    "aaa"
}

fun CoroutineScope.getUserFromLocal(login: String) = async(Dispatchers.IO) {
    "bbb"
}