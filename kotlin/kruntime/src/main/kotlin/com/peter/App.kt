package com.peter

import kotlinx.coroutines.*
import java.util.concurrent.atomic.AtomicLong
import kotlin.system.measureTimeMillis

//第一个协程程序

//fun main() {
//    GlobalScope.launch(Dispatchers.Main) {
//        delay(1000L)
//        println("World!")
//        println(
//            Thread.currentThread().id
//        )
//    }
//    println("Hello,")
//    println(
//        Thread.currentThread().id
//    )
//    Thread.sleep(2000L)
//    println(
//        Thread.currentThread().id
//    )
//}

/**
 *本质上，协程是轻量级的线程。
 * 它们在某些 CoroutineScope 上下文中与 launch 协程构建器 一起启动。
 * 这里我们在 GlobalScope 中启动了一个新的协程，这意味着新协程的生命周期只受整个应用程序的生命周期限制。
 *
 * 这是因为 delay 是一个特殊的 挂起函数 ，它不会造成线程阻塞，但是会 挂起 协程，并且只能在协程中使用。
 *
 * 它启动了 10 万个协程，并且在 5 秒钟后，每个协程都输出一个点。
import kotlinx.coroutines.*

fun main() = runBlocking {
repeat(100_000) { // 启动大量的协程
launch {
delay(5000L)
print(".")
}
}
}
 */


/**
 * 以上代码就涉及到了协程的四个基础概念：

suspend function。即挂起函数，delay() 就是协程库提供的一个用于实现非阻塞式延时的挂起函数
CoroutineScope。即协程作用域，GlobalScope 是 CoroutineScope 的一个实现类，用于指定协程的作用范围，可用于管理多个协程的生命周期，所有协程都需要通过 CoroutineScope 来启动
CoroutineContext。即协程上下文，包含多种类型的配置参数。Dispatchers.IO 就是 CoroutineContext 这个抽象概念的一种实现，用于指定协程的运行载体，即用于指定协程要运行在哪类线程上
CoroutineBuilder。即协程构建器，协程在 CoroutineScope 的上下文中通过 launch、async 等协程构建器来进行声明并启动。launch、async 均被声明为 CoroutineScope 的扩展方法




运行一个新的协程并阻塞当前线程直到它完成。不应在协程中使用此函数。它旨在将常规阻塞代码连接到以挂起样式编写的库，以用于main函数和测试。


launches a new coroutine without blocking the current thread and returns a reference to the coroutine as a Job.
The coroutine is cancelled when the resulting job is cancelled.


 */

suspend fun doSomethingUsefulOne(): Int {
    delay(1000L) // pretend we are doing something useful here
    return 13
}

suspend fun doSomethingUsefulTwo(): Int {
    delay(1000L) // pretend we are doing something useful here, too
    return 29
}

fun main() = runBlocking {
    val start = System.currentTimeMillis()
    println("start")
    val c = AtomicLong()

    val sss = hashSetOf<Long>()

    val useTime = measureTimeMillis {
        for (i in 1..1_000_000L)
            GlobalScope.launch {
                sss.add(Thread.currentThread().id)
                println(sss)
                c.addAndGet(i)
            }
    }


    println("end，useTime = ${useTime}，sum = ${c.get()}")
}

suspend fun failedConcurrentSum(): Int = coroutineScope {
    val one = async<Int> {
        try {
            delay(Long.MAX_VALUE) // Emulates very long computation
            42
        } finally {
            println("First child was cancelled")
        }
    }
    val two = async<Int> {
        println("Second child throws an exception")
        throw ArithmeticException()
    }
    one.await() + two.await()
}

suspend fun concurrentSum(): Int = coroutineScope {
    val one = async { doSomethingUsefulOne() }
    val two = async { doSomethingUsefulTwo() }
    one.await() + two.await()
}