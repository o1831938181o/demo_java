package pkg.ddelegation

interface Base1 {
    fun printMessage()
    fun printMessageLine()
}

class BaseImpl1(val x: Int) : Base1 {
    override fun printMessage() {
        print(x)
    }

    override fun printMessageLine() {
        println(x)
    }
}

class Derived1(b: Base1) : Base1 by b {
    override fun printMessage() {
        print("abc")
    }
}

fun main() {
    val b = BaseImpl1(12)
    val b1 = Derived1(b)
    val b2 = Derived1(b)

    println()
}