package pkg.delegate

import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class Example {
    var p: String by Delegate()
}

fun main() {
    val example = Example()

    /**
     * 方法里面的属性委托
     */
    var p1: String by Delegate()

//    p1 = "dfgh"

    println(p1)
    println(example.p)
    println(p1)
//    example.p = "dfdg"
//    println(example.p)
}

// 委托的类
class Delegate {

    var value = "567465"
        get() = "xxx"
        set(value) {
            field = "sf"
        }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, 这里委托了 ${property.name} 属性"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$thisRef 的 ${property.name} 属性赋值为 $value")
    }
}

class Delegate1 {
    fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "xxg"
    }

    fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value has been assigned to ${property.name} in $thisRef")
    }
}

class A : ReadWriteProperty<Any?, String?> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): String? {
        return "aaaa"
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: String?) {
        println("setValue=$value")
    }

}

class B : ReadOnlyProperty<Any?, String?> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}