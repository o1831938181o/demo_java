#include <iostream>

int main() {
    int n = 0, i = 2;
    switch (0) {
        case 0:
            std::cout << "///" << std::endl;
            do {
                case 1:
                    std::cout << " ----" << std::endl;
                    n++;
                case 2:
                    std::cout << "===" << std::endl;
                    n++;
            } while (--i > 0);
            break;
    }

    std::cout << "n = " << n << std::endl;
    return 0;
}
