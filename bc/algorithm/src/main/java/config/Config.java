package config;

import lombok.Data;
import service.Service;

import java.util.List;

/**
 * the system configs
 */
@Data
public class Config {

    /**
     * the value threshold that value 0 < x <= 1
     */
    private float threshold;

    /**
     * a time interval
     */
    private long time_interval;

    /**
     * a service ID
     */
    private int SID;

    /**
     * the cnt of the nodes
     */
    private int NODE_COUNT;

    /**
     * if we want to query the data,we first should get the keys so that we can fnd this node
     */
    private List<String> NODES_KEYS;

    /**
     * the service represent this config
     */
    private Service service;

    /**
     * @param SID the service id
     */
    public void config(int SID) {
        int default_threshold = -1;
        int default_time_interval = -1;
        config(default_threshold, SID, default_time_interval);
    }

    /**
     * @param threshold     value 0 < x <= 1
     * @param SID           service id
     * @param time_interval time interval
     */
    public void config(int threshold, int SID, int time_interval) {
        this.threshold = threshold;
        this.SID = SID;
        this.time_interval = time_interval;
    }
}
