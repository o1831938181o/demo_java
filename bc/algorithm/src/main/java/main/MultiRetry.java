package main;

import config.Config;
import query.Partition;
import utils.Response;

public class MultiRetry {
    static int RetryCnt;

    static {
        RetryCnt = 5;
    }

    public void queryWithTry(Config config) {
        for (int i = 0; i < RetryCnt; i++) {
            Response res = query(config);
            if (Response.SUCCESS_CODE == (Integer) res.get("code")) {
                System.out.println("=========" + i + "=========");
                System.out.println(res);
                break;
            }
        }
    }

    public Response query(Config config) {
        var app = new Partition(config);
        return app.domMethod();
    }
}
