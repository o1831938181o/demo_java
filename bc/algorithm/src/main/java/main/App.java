package main;

import config.Config;

public class App {
    private final static Config config;

    static {
        config = new Config();
        initConfig();
    }


    private static void initConfig() {
        config.setSID(1);
        config.setThreshold(0.8f);
        config.setTime_interval(1000);
    }

    public static void main(String[] args) {
        MultiRetry multiRetry = new MultiRetry();
        multiRetry.queryWithTry(config);
    }

}
