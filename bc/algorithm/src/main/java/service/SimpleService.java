package service;

import com.google.gson.Gson;
import config.Config;
import entity.Node;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

public class SimpleService extends AbsService {

    /**
     * gson to standardize
     */
    volatile Gson gson = new Gson();


    public SimpleService(int id, Config config) {
        super(id, config);
    }

    /**
     * @param key the node-key
     * @return the node represent the node value of the given key
     */
    @Override
    public Node query(String key) {
        Node node = null;
        try (BufferedReader br = new BufferedReader(new FileReader("/Volumes/T7/demo_java/bc/algorithm/src/main/resources/" + key + ".json"))) {
            while (!br.ready()) ;
            Map<String, Object> m = gson.fromJson(br, Map.class);
            node = new Node(key, m.get("data").toString());
            System.out.println(node);
            System.out.println("---");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return node;
    }

    /**
     * @return the ids represent the node keys
     */
    @Override
    public String[] ids() {
        int sid = getSid();
        // simulation for only for a test
        return new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    }
}
