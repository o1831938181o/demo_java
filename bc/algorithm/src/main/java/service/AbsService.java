package service;

import config.Config;
import lombok.Getter;

public abstract class AbsService implements Service {

    /**
     * config support for the future
     */
    protected final Config config;

    /**
     * the SID
     */
    @Getter
    private final int sid;

    public AbsService(int sid, Config config) {
        this.sid = sid;
        this.config = config;
    }
}
