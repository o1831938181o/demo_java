package service;

import config.Config;
import entity.Node;
import service.extension.couchdb.CouchDbExtension;

public class CouchService extends AbsService {

    private final CouchDbExtension dbExtension;

    public CouchService(int sid, Config config) {
        super(sid, config);
        dbExtension = new CouchDbExtension(config);
    }

    @Override
    public Node query(String key) {
        return dbExtension.query(key);
    }

    @Override
    public String[] ids() {
        return dbExtension.ids();
    }
}
