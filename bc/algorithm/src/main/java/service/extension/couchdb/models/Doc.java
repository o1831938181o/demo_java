package service.extension.couchdb.models;

import lombok.Data;

@Data
public class Doc {
    private String _id;
    private String _rev;

    // custom specific
    private int code;
    private String message;
    private Data data;

    @lombok.Data
    public static class Data {
        private int id;
        private String name;
        private String bfirstletter;
        private String logo;
        private String country;
        private int countryid;
    }
}