package service.extension.couchdb;

import com.google.gson.Gson;
import config.Config;
import entity.Node;
import org.apache.commons.io.FileUtils;
import service.extension.couchdb.models.AllDocs;
import service.extension.couchdb.models.Doc;
import utils.DbUtils;
import utils.JsonUtils;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * extension for couch db , for add a default
 * if you are watch this ,it's better for u to make a file like this
 */
public class CouchDbExtension {

    private static Gson gson = JsonUtils.gson;

    private final Config config;

    private final DbUtils dbUtils;

    public CouchDbExtension(Config config) {
        this.config = config;
        dbUtils = new DbUtils(config);
    }

    /**
     * {
     * "ok": true
     * }
     */
    public void createDb() {
        try (var res = dbUtils.createDb("cars")) {
            if (res.body() != null) {
                try {
                    System.out.println(res.body().string());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * find doc with the filter condition (case find all docs is useless for the case)
     */
    public void findAllFilterDoc() {

    }

    public void filterFind() {

    }


    /**
     * add bulk docs in one http
     */
    public void addDocs() {

        String data = null;
        try {
            String all = Objects.requireNonNull(this.getClass().getClassLoader().getResource("all.json")).getFile();
            data = FileUtils.readFileToString(new File(all), "utf-8");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (data != null) {
            try (var res = dbUtils.addBulkDoc("cars", data)) {
                if (res.body() != null) {
                    try {
                        System.out.println(res.body().string());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }

    }

    /**
     * add single-doc in one http
     */
    public void addDoc() {

    }

    public void findSpecDoc(String docId) {
        try (var res = dbUtils.findSpecDoc("cars", docId)) {
            if (res.body() != null) {
                try {
                    System.out.println(res.body().string());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /* -----------------------------------API Start----------------------------------------*/

    /**
     * @return ids we are need to find the doc , may return null
     */
    public String[] ids() {
        String[] keys = null;
        try (var res = dbUtils.allDocs("cars")) {
            if (res.body() != null) {
                try {
                    var body = res.body().string();
                    var doc = gson.fromJson(body, AllDocs.class);
                    keys = new String[doc.getTotal_rows()]; //init the keys
                    for (int i = 0; i < doc.getRows().size(); i++) {
                        keys[i] = doc.getRows().get(i).getId();
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return keys;
    }

    public Node query(String key) {
        Node node = null;
        try (var res = dbUtils.findSpecDoc("cars", key)) {
            if (res.body() != null) {
                try {
                    var body = res.body().string();
                    Doc doc = gson.fromJson(body, Doc.class);
                    Doc.Data data = doc.getData();
                    node = new Node(key, data.toString());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return node;
    }


    /* -----------------------------------API End----------------------------------------*/

    public static void main(String[] args) {
        CouchDbExtension couchDbExtension = new CouchDbExtension(null);
//        couchDbExtension.createDb();
//        couchDbExtension.addDocs();

//        couchDbExtension.findSpecDoc("a");

        var ans = couchDbExtension.ids();

        for (String an : ans) {
            System.out.println(an);
        }


        var node = couchDbExtension.query("a");

        System.out.println(node.value());
        System.out.println(node.key());

//        try (var res = couchDbExtension.dbUtils.uuids(5)) {
//            if (res.body() != null) {
//                try {
//                    System.out.println(res.body().string());
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//
//        }

//        try (var res = couchDbExtension.dbUtils.allDocs("cars")) {
//            if (res.body() != null) {
//                try {
//                    System.out.println(res.body().string());
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        }

    }
}
