package service.extension.couchdb.models;

import lombok.Data;

import java.util.List;

@Data
public class AllDocs {
    private int total_rows;
    private int offset;
    private List<Container> rows;


    @Data
    public static
    class Container {
        String id;
        String key;
        REV value;

        @Data
        static
        class REV {
            String rev;
        }
    }
}
