package service;

import config.Config;
import lombok.NonNull;

/**
 * class for access the service
 */
public class ServiceHelper {
    private static Service service;

    private static ServiceType mType;

    public static Service getService(int sid, @NonNull ServiceType type, Config config) {
        if (service == null || mType != type) createService(sid, type, config);
        return service;
    }

    private static void createService(int sid, @NonNull ServiceType type, Config config) {
        if (sid == -1) throw new IllegalArgumentException("wrong sid");
        switch (type) {
            case DEFAULT -> {
                service = new SimpleService(sid, config);
                mType = type;
            }
            case TYPE_COUCHDB -> {
                service = new CouchService(sid, config);
                mType = type;
            }
            case TYPE_2 -> System.out.println("TYPE_2");
            default -> System.out.println("default");
        }
    }
}
