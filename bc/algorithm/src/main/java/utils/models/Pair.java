package utils.models;

import lombok.Data;

@Data
public class Pair {
    private int m;
    private float λ;
    private double θ;
}
