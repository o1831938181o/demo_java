package utils;

import config.Config;
import lombok.NonNull;

import java.util.Arrays;
import java.util.Random;

public class ShuffleUtils {

    private static Random rand;

    public static String[] randomChoose(String[] ids, @NonNull Config config) {
        shuffle(ids);
        // cut idx what we should query
        int cut_idx = rand.nextInt(ids.length);
        return Arrays.copyOf(ids, cut_idx);
    }


    private static <T> void swap(T[] a, int i, int j) {
        T temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static <T> void shuffle(T[] arr) {
        int length = arr.length;
        rand = new Random(); // init the random to fit the random
        for (int i = length; i > 0; i--) {
            int randInd = rand.nextInt(i);
            swap(arr, randInd, i - 1);
        }
    }
}
