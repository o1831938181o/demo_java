package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public class JsonUtils {

    public final static Gson gson;
    public final static Type mType;

    static {
        gson = new Gson();
        mType = new TypeToken<Map<String, String>>() {
        }.getType();
    }
}
