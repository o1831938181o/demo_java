package query;

import config.Config;
import entity.Node;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import service.Service;
import service.ServiceHelper;
import service.ServiceType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Query {
    /**
     * the config
     */
    @NonNull
    @Getter
    private final Config config;

    /**
     * the service
     */
    @Getter
    private Service service;

    public Query(@NonNull Config config) {
        this.config = config;
    }

    /**
     * temporary define the thread counts
     */
    private static final int DEFAULT_LEN = 12;

    /**
     * @param keys the given query keys
     * @return we find all the data,and prepare the partition
     */
    @SneakyThrows
    List<Node> queries(List<String> keys) {
        // fist it should load the service
        loadService();
        ExecutorService pool = Executors.newFixedThreadPool(DEFAULT_LEN);
        // query
        List<Node> query = new ArrayList<>();
        // use the countdownLatch
        final CountDownLatch latch = new CountDownLatch(keys.size());
        keys.forEach(key -> pool.submit(() -> {
            Node node = service.query(key);
            if (node != null) {
                query.add(node);
            }
            latch.countDown();
        }));
        // await
        latch.await();
        System.out.println(query);
        // shutdown the pool
        if (!pool.isShutdown()) pool.shutdown();
        return query;
    }

    /**
     * @return key ids the node we are monitoring
     */
    public String[] ids() {
        // fist it should load the service
        loadService();
        return service.ids();
    }

    /**
     * load the service we are calling
     */
    public void loadService() {
        if (!isNull()) return;
        service = ServiceHelper.getService(config.getSID(), ServiceType.TYPE_COUCHDB, config);
        config.setService(service);
    }

    /**
     * @return true if the service ,false service is not null
     */
    private boolean isNull() {
        return service == null;
    }
}
