package query;

import config.Config;
import entity.Node;
import lombok.Getter;
import lombok.NonNull;
import utils.MD5Utils;
import utils.QuickQueue;
import utils.Response;
import utils.ShuffleUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Partition {

    /**
     * the ids that the user can reach ,so it should be recognized as keys
     */
    private String[] ids;

    @Getter
    @NonNull
    private final Config config;

    @Getter
    private Query query;

    public Partition(@NonNull Config config) {
        this.config = config;
    }

    /**
     * the core method
     */
    public Response domMethod() {
        // init the query
        query = new Query(config);
        // init the service and make the config set the service (opt)
        query.loadService();
        // we find all the ids
        if (ids == null) ids = query.ids();
        // then we need to get cut ids
        String[] cut_ids = ShuffleUtils.randomChoose(ids, config);
        // we then find the answers
        List<Node> queries = query.queries(Arrays.stream(cut_ids).toList());

        // analyze
        return analyze(queries);
    }

    /**
     * @param queries the given queries to analyze
     */
    private Response analyze(List<Node> queries) {
        QuickQueue quickQueue = new QuickQueue();

        float len = queries.size();
        System.out.println(len);

        // data 数据
        queries.forEach(query -> {
            quickQueue.inc(query.value());
        });

        int MAX_CNT = quickQueue.getMaxKeyCnt();
        String DATA = quickQueue.getMaxKey();
        String en = MD5Utils.md5(DATA);

        float percent = MAX_CNT / len;

        System.out.println(MAX_CNT);

        if (percent < 0.5f) {
            return Response.ErrorCode();
        }

        if (percent < config.getThreshold()) {
            return Response.WeakErrorCode();
        }

        // then the answer is completely be trust
        Map<String, String> m = new HashMap<>();
        m.put(en, DATA); //  but why

        return Response.SuccessCode(m);
    }
}
