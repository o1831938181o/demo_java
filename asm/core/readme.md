> https://lsieun.github.io/java-asm-01/method-initial-frame.html
> 
> 7. 总结
     本文主要介绍了MethodVisitor类的示例，内容总结如下：

第一点，要注意MethodVisitor类里visitXxx()的调用顺序
第一步，调用visitCode()方法，调用一次
第二步，调用visitXxxInsn()方法，可以调用多次
第三步，调用visitMaxs()方法，调用一次
第四步，调用visitEnd()方法，调用一次
第二点，在.class文件当中，构造方法的名字是<init>，静态初始化方法的名字是<clinit>。
第三点，针对方法里包含的Instruction内容，需要放到Frame当中才能更好的理解。对每一条Instruction来说，它都有可能引起local variables和operand stack的变化。
第四点，在使用COMPUTE_FRAMES的前提下，我们可以给visitMaxs()方法参数传入错误的值，但不能忽略对于visitMaxs()方法的调用。
第五点，不同的MethodVisitor对象，它们的visitXxx()方法是彼此独立的，只要各自遵循方法的调用顺序，就能够得到正确的结果。
最后，本文列举的代码示例是有限的，能够讲到visitXxxInsn()方法也是有限的。针对于某一个具体的visitXxxInsn()方法，我们可能不太了解它的作用和如何使用它，这个是需要我们在日后的使用过程中一点一点积累和熟悉起来的。

```text
https://lsieun.github.io/java-asm-01/label-intro.html
如何使用Label类：

首先，创建Label类的实例；
其次，确定label的位置。通过MethodVisitor.visitLabel()方法，确定label的位置。
最后，与label建立联系，实现程序的逻辑跳转。在条件合适的情况下，通过MethodVisitor类跳转相关的方法（例如，visitJumpInsn()）与label建立联系。
举个形象的例子，在《火影忍者》当中，飞雷神之术的特点：

第一步，掏出两把带有飞雷神标记的“苦无”。
第二步，将两把“苦无”扔到指定位置。
第三步，使用时空间忍术跳转到某一个“苦无”的位置。
```

