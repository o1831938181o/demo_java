package basic.utils;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.ASMifier;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 这里的代码是参考自{@link Printer# main}
 */
public class ASMPrint {
    public static void main(String[] args) throws IOException {
//        // (1) 设置参数
//        String className = "sample.HelloWorld";
//        int parsingOptions = ClassReader.SKIP_FRAMES | ClassReader.SKIP_DEBUG;
//        boolean asmCode = true;
//
//        // (2) 打印结果
//        Printer printer = asmCode ? new ASMifier() : new Textifier();
//        PrintWriter printWriter = new PrintWriter(System.out, true);
//        TraceClassVisitor traceClassVisitor = new TraceClassVisitor(null, printer, printWriter);
//
//        IOUtils.getClassReader(new File("./HelloWorld.class")).accept(traceClassVisitor, parsingOptions);


        System.gc();
//        System.gc();

        Runtime runtime = Runtime.getRuntime();
        System.out.println("D8 is running with total memory:" + runtime.totalMemory());
        System.out.println("D8 is running with free memory:" + runtime.freeMemory());
        System.out.println("D8 is running with max memory:" + runtime.maxMemory());
    }
}
