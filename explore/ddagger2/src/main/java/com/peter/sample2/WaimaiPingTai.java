package com.peter.sample2;

import com.peter.sample1.ZhaiNan;
import com.peter.sample3.Activity;
import com.peter.sample3.PageScope;
import com.peter.sample4.Activity2;
import com.peter.sample4.ActivityModule;
import com.peter.sample5.TestLazy;
import dagger.Component;

import javax.inject.Singleton;

@PageScope
@Component(modules = {ShangjiaAModule.class, ActivityModule.class})
public interface WaimaiPingTai {
    ZhaiNan waimai();

    /**
     * inject it itself
     **/
    void inject(Activity mActivity);

    void injectActivity2(Activity2 activity2);


    void injectLazy(TestLazy testLazy);
}

