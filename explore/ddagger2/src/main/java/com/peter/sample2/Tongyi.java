package com.peter.sample2;

import com.peter.sample1.Noodle;
import com.peter.sample3.PageScope;

import javax.inject.Inject;
import javax.inject.Singleton;


@PageScope
public class Tongyi extends Noodle {

    @Inject
    public Tongyi() {
    }

    @Override
    public String toString() {
        return "统一方便面";
    }
}