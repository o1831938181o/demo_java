package com.peter.sample2;

import com.peter.sample1.Baozi;
import com.peter.sample1.Noodle;
import dagger.Module;
import dagger.Provides;

@Module
public class ShangjiaAModule {

    @Provides
    public Baozi provideBaozi() {
        return new Baozi("豆沙包");
    }

    @Provides
    public Kangshifu provideKangshifu() {
        return new Kangshifu();
    }

    /**
     * same as
     */
    @Provides
    public Noodle provideNoodle(Kangshifu noodle) {
        return noodle;
    }

//    @Provides
//    public Noodle provideNoodle(Tongyi noodle) {
//        return noodle;
//    }
}
