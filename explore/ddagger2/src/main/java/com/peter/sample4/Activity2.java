package com.peter.sample4;

import com.peter.sample2.DaggerWaimaiPingTai;

import javax.inject.Inject;
import javax.inject.Named;

public class Activity2 {
    @Inject
    @Named("phone")
    String phone;

    @Inject
    @Named("computer")
    String computer;

    @Inject
    @Phone
    String phone1;

    @Inject
    @Computer
    String computer1;

    @Override
    public String toString() {
        return "Activity2{" +
                "phone='" + phone + '\'' +
                ", computer='" + computer + '\'' +
                ", phone1='" + phone1 + '\'' +
                ", computer1='" + computer1 + '\'' +
                '}';
    }
}

class App {
    public static void main(String[] args) {
        var activity2 = new Activity2();
        DaggerWaimaiPingTai.builder().build().injectActivity2(activity2);
        System.out.println(activity2.toString());
    }
}
