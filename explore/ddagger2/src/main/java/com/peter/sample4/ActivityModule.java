package com.peter.sample4;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public class ActivityModule {

    @Provides
    @Named("phone")
    public String providePhone() {
        return "手机";
    }

    @Provides
    @Named("computer")
    public String provideComputer() {
        return "电脑";
    }

    @Provides
    @Phone
    public String providePhone1() {
        return "手机1";
    }

    @Provides
    @Computer
    public String provideComputer1() {
        return "电脑1";
    }
}
