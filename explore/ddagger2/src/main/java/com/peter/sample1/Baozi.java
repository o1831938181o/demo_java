package com.peter.sample1;

import javax.inject.Inject;

public class Baozi {

    String name = "小笼包";


    @Inject
    public Baozi() {
    }

    public Baozi(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}
