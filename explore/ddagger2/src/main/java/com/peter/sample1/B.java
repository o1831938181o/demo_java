package com.peter.sample1;

import javax.inject.Inject;

public class B {
    int value;

    @Inject
    public B(int value) {
        this.value = value;
    }

}
