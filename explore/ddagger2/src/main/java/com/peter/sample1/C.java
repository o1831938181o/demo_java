package com.peter.sample1;

import javax.inject.Inject;

public class C {
    int d;
    String e;

    @Inject
    public C(int index, String value) {
        this.d = index;
        this.e = value;
    }
}