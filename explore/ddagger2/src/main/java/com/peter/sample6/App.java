package com.peter.sample6;

public class App {
    public static void main(String[] args) {
        CarComponent carComponent = DaggerCarComponent.create();
        Car car1 = carComponent.car();
        System.out.println(car1.getEngine());
    }
}
