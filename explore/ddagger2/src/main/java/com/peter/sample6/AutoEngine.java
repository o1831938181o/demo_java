package com.peter.sample6;

import javax.inject.Inject;

public class AutoEngine extends Engine {
    //为构造方法添加注解，支持注入
    @Inject
    public AutoEngine() {
    }
}
