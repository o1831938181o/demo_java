package com.peter.sample6;

import dagger.Component;

@Component(modules = EngineMoudle.class)
public interface CarComponent {
    Car car();
}

