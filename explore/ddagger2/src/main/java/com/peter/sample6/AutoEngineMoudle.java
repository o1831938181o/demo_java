package com.peter.sample6;

import dagger.Binds;
import dagger.Module;

@Module
public interface AutoEngineMoudle {
    @Binds
    Engine bindEngine(AutoEngine autoEngine);
}

