package com.peter.sample6;

import dagger.Binds;
import dagger.Module;

@Module
public interface FocusEngineMoudle {
    @Binds
    Engine bindEngine(FocusEngine focusEngine);
}
