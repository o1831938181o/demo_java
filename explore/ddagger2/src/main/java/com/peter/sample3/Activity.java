package com.peter.sample3;

import com.peter.sample2.DaggerWaimaiPingTai;
import com.peter.sample2.Kangshifu;
import com.peter.sample2.Tongyi;

import javax.inject.Inject;

public class Activity {
    @Inject
    Tongyi tonyi;

    @Inject
    Tongyi tonyi1;

    @Inject
    Kangshifu kangshifu;

    public Activity() {
    }

    @Override
    public String toString() {
        System.out.println(tonyi.hashCode());
        System.out.println(tonyi1.hashCode());
        return "Activity{" +
                "tonyi=" + tonyi +
                ", kangshifu=" + kangshifu +
                '}';
    }
}


class App {
    public static void main(String[] args) {
        Activity activity = new Activity();
        DaggerWaimaiPingTai.builder().build().inject(activity);
        System.out.println(activity);
    }
}
