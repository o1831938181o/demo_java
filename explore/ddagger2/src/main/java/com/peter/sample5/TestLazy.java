package com.peter.sample5;

import com.peter.sample2.DaggerWaimaiPingTai;
import dagger.Lazy;

import javax.inject.Inject;
import javax.inject.Named;

public class TestLazy {

    @Inject
    @Named("computer")
    Lazy<String> computer;


    public static void main(String[] args) {
        var testAc = new TestLazy();
        DaggerWaimaiPingTai.builder().build().injectLazy(testAc);
        System.out.println();
    }
}
