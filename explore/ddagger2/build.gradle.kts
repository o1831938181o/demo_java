import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.6.20"
    kotlin("kapt") version "1.7.20"
}

group = "org.peter"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    // https://mvnrepository.com/artifact/com.google.dagger/dagger
    implementation("com.google.dagger:dagger:2.43.2")
    annotationProcessor("com.google.dagger:dagger-compiler:2.43.2")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.0")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}