package org.example;

import javax.inject.Inject;

public class Noodle1 implements INoodle1 {

    @Inject
    public Noodle1() {

    }

    @Override
    public String toString() {
        return "Noodle1";
    }
}
