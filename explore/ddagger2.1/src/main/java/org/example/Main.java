package org.example;

import org.example.ssub.Child;
import org.example.ssub.DaggerParentComponent;
import org.example.ssub.ParentComponent;

import javax.inject.Inject;

public class Main {

    public static void main(String[] args) {
        final ZhaiNan zhaiNan = DaggerComponent.builder().build().waimai();
        System.out.println(zhaiNan.iNoodle1);

        ParentComponent parentComponent = DaggerParentComponent.builder().build();

        Child c = new Child();

        parentComponent.childComponent().build().inject(c);


        System.out.println();
//        parentComponent.childComponent().build();
    }
}