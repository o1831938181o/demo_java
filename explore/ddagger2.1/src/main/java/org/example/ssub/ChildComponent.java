package org.example.ssub;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

@Subcomponent(modules = ChildComponent.BikeModule.class)
public interface ChildComponent {

    @Subcomponent.Builder
    interface Builder {
        ChildComponent build();
    }

    void inject(Child child);

    @Module
    class BikeModule {

        @Provides
        public Bike provideBike() {
            return new Bike();
        }
    }
}
