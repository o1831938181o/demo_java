package org.example.ssub;

import dagger.Component;
import dagger.Module;
import dagger.Provides;

@Component(modules = ParentComponent.CarModule.class)
public interface ParentComponent {
    ChildComponent.Builder childComponent();

    @Module(subcomponents = ChildComponent.class)
    class CarModule {

        @Provides
        public Car provideCar() {
            return new Car();
        }
    }
}
