package org.example.ssub;

import javax.inject.Inject;

public class Child {
    @Inject
    Car car;

    @Inject
    Bike bike;
}
