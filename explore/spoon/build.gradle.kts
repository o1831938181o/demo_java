plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.6.20"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    // https://mvnrepository.com/artifact/fr.inria.gforge.spoon/spoon-core
    implementation("fr.inria.gforge.spoon:spoon-core:10.1.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}