import androidx.compose.material.MaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application

@Composable
@Preview
fun App() {
    var text: String by remember { mutableStateOf("Hello, World!") }

    println(text)

    MaterialTheme {
        Button(onClick = {
            text = "Hello, Desktop!"
            println()
        }) {
            Text(text)
        }
    }


}

fun main() = application {
    Window(onCloseRequest = ::exitApplication) {
        App()
    }
}
